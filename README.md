## PHP-FPM Image

 **Helpful PHP-FPM image from official ubuntu:xenial**
 >
 > PHP-FPM version - 7.3

 > DateTime - Europe/Kiev

 > Composer installed globally

### Extensions:

 * php7.3-pgsql
 * php7.3-mysql
 * php7.3-opcache
 * php7.3-common
 * php7.3-mbstring
 * php7.3-soap
 * php7.3-cli
 * php7.3-intl
 * php7.3-json
 * php7.3-xsl
 * php7.3-imap
 * php7.3-ldap
 * php7.3-curl
 * php7.3-gd
 * php7.3-zip
 * php7.3-fpm
 * php7.3-redis
 * php-memcached
 * php-mongodb
 * php7.3-imagick
 * php7.3-bcmath
 * php-zmq
 * php7.3-apcu
 * php7.3-sqlite
 * php7.3-sqlite3
 * sqlite3

### In addition

 * Composer (installed globally)
 * Cron
 * Browscap ([Browscap official page](http://browscap.org/))
 * wkhtmltopdf ([Official website](https://wkhtmltopdf.org/))
 
### Docker-compose usage (example)

```yaml
version: "2"
services:
 nginx:
   image: nginx:1.16
   depends_on:
    - php-fpm
   links:
    - php-fpm
   environment:
    - NGINX_PORT=80
    - FASTCGI_HOST=php-fpm
    - FASTCGI_PORT=9000
    - DOCUMENT_ROOT=/usr/local/src/app/web # ROOT folder for Symfony framework
   ports:
    - 8600:80
   volumes:
    - ./stack/nginx/templates/default.conf.template:/etc/nginx/conf.d/default.conf.template
    - ./stack/nginx/entrypoint.sh:/entrypoint.sh
   volumes_from:
    - php-fpm
   command: "/bin/bash /entrypoint.sh"

 database:
   image: mysql:5.7
   environment:
     MYSQL_ROOT_PASSWORD: root_password
     MYSQL_DATABASE: app
     MYSQL_USER: app
     MYSQL_PASSWORD: password
   ports:
    - 3309:3306
   volumes:
    - data:/var/lib/mysql

 php-fpm:
   image: itdevgroup/php-fpm7.3
   depends_on:
    - database
   links:
    - database
   volumes:
    - .:/usr/local/src/app
   working_dir: /usr/local/src/app
   extra_hosts:
    - "app:127.1.0.1"
   environment:
     DB_HOST: database
     DB_PORT: 3306
     DB_DATABASE: app
     DB_USERNAME: app
     DB_PASSWORD: password
volumes:
 data: {}
```
